#!/usr/bin/env python3

"""
Script for the analysis of metrological surveys of bent chips performed with the Keyence profilometer
Run script: python fit_ITS3_chip.py config.yml
"""

import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import yaml
#from sklearn.cluster import dbscan
from mpl_toolkits.mplot3d import Axes3D
from cylinder_fitting import fit
from cylinder_fitting.geometry import point_line_distance, normalize, rotation_matrix_from_axis_and_angle
from lmfit.models import GaussianModel
from sklearn.cluster import dbscan


def show_fit(w_fit, C_fit, r_fit, Xs, w_fit_perp):
    """
    Plot the fitting given the fitted axis direction, the fitted center
    the fitted radius and the data points
    """

    fig = plt.figure(figsize=(15, 7))
    ax = fig.gca(projection="3d")

    # Plot the data points
    ax.scatter([X[0] for X in Xs], [X[1] for X in Xs], [X[2] for X in Xs])

    # Get the transformation matrix
    theta = np.arccos(np.dot(w_fit, np.array([0, 0, 1])))
    phi = np.arctan2(w_fit[1], w_fit[0])

    M = np.dot(rotation_matrix_from_axis_and_angle(np.array([0, 0, 1]), phi),
               rotation_matrix_from_axis_and_angle(np.array([0, 1, 0]), theta))

    # Plot the cylinder surface
    delta = np.linspace(-np.pi, np.pi, 20)
    z = np.linspace(-10, 10, 20)

    Delta, Z = np.meshgrid(delta, z)
    X = r_fit * np.cos(Delta)
    Y = r_fit * np.sin(Delta)

    for i in range(len(X)):
        for j in range(len(X[i])):
            p = np.dot(M, np.array([X[i][j], Y[i][j], Z[i][j]])) + C_fit

            X[i][j] = p[0]
            Y[i][j] = p[1]
            Z[i][j] = p[2]

    ax.plot_surface(X, Y, Z, alpha=0.2)

    # Plot the center and direction
    ax.quiver(C_fit[0], C_fit[1], C_fit[2],
              r_fit * w_fit[0], r_fit * w_fit[1], r_fit * w_fit[2], color="red")

    ax.quiver(C_fit[0], C_fit[1], C_fit[2],
              r_fit * w_fit_perp[0], r_fit * w_fit_perp[1], r_fit * w_fit_perp[2], color="blue")

    return fig, ax


def read_file_Keyence(file_name, x_min, x_max, y_min, y_max, xmax_window, ymax_window,
                      sampling_frac, remove_outliers):
    """
    Helper function to read Keyence files
    """

    df = pd.read_csv(file_name, sep=",", skiprows=23,
                     header=None, dtype="string")
    df.replace("<NA>", np.nan, regex=True, inplace=True)
    df = df.astype(float, errors="ignore").fillna(-1.e9)

    n_rows, n_cols = df.shape
    y_step = ymax_window / n_rows
    x_step = xmax_window / n_cols
    x, y, z, = ([] for _ in range(3))

    for i_row, row in df.iterrows():
        for i_col, el in enumerate(row.astype(float).to_numpy()):
            if el > -1.e9:
                x.append((i_col+1) * x_step)
                y.append((i_row+1) * y_step)
                z.append(el)

    df_out = pd.DataFrame({"x": x, "y": y, "z": z})
    df_out.query(
        f"{x_min} < x < {x_max} and {y_min} < y < {y_max}", inplace=True)

    # remove outliers with dbscan
    if remove_outliers:
        _, clLabelsRel = dbscan(
            df_out.to_numpy(), eps=0.1, min_samples=50, metric="euclidean")
        df_out["cluster"] = clLabelsRel
        df_out.query("cluster >= 0", inplace=True)
        # remove also small clusters
        for cls in np.unique(df_out["cluster"].to_numpy()):
            if list(df_out["cluster"].to_numpy()).count(cls) / len(df_out) < 1.e-3:
                df_out.query(f"cluster != {cls}", inplace=True)

    df_out = df_out.sample(
        frac=sampling_frac, random_state=42, replace=True, axis=0)
    return df_out


def main(file_name, x_min, x_max, y_min, y_max, xmax_window, ymax_window,
         save_figures, save_res, sampling_frac=1., remove_outliers=True):
    """
    Main function that performs the analysis
    """

    df = read_file_Keyence(file_name, x_min, x_max, y_min, y_max,
                           xmax_window, ymax_window, sampling_frac, remove_outliers)

    x = df["x"].to_numpy()
    y = df["y"].to_numpy()
    z = df["z"].to_numpy()
    y_mean = np.mean(y)

    data, data_for_fit = [], []
    for xx, yy, zz in zip(x, y, z):
        data.append(np.array([xx, yy, zz]))
        if y_mean-10 < yy < y_mean+10:
            data_for_fit.append(np.array([xx, yy, zz]))

    w_fit, C_fit, r_fit, _ = fit(data_for_fit)
    # define arbitrary vector perpendicular to cylinder axis
    w_fit_perp = normalize(np.array([-1., w_fit[0]/w_fit[1], 0.]))

    res_radial, rphi, axial_dist = [], [], []
    for point in data:
        dist = point_line_distance(point, C_fit, w_fit)
        res_radial.append((dist-r_fit) * 1000)
        u = point - C_fit
        axial_dist.append(np.dot(u, normalize(w_fit)))
        axial_vec = w_fit * axial_dist[-1]
        axial_point = C_fit + axial_vec
        radius = np.linalg.norm(point - axial_point)
        phi = np.arccos(np.dot(w_fit_perp, normalize(point - axial_point)))
        rphi.append(phi*radius)
    res_radial = np.array(res_radial)

    df_residuals = pd.DataFrame(
        {"residuals": res_radial, "z": axial_dist, "rphi": rphi})

    fig_scatter = plt.figure(figsize=(15, 7))
    ax_scatter = Axes3D(fig_scatter)
    ax_scatter.scatter(x, y, z, cmap="coolwarm")
    ax_scatter.set_xlabel("x (mm)")
    ax_scatter.set_ylabel("y (mm)")
    ax_scatter.set_zlabel("z (mm)")

    fig_res_distr = plt.figure(figsize=(7, 7))
    histo = plt.hist(res_radial, bins=200)
    contents = histo[0]
    bins = histo[1]
    binwidth = bins[1]-bins[0]
    bin_cent = [edge + binwidth/2 for edge in bins[:-1]]
    gaus_res_radial = GaussianModel()
    result_gaus = gaus_res_radial.fit(contents, x=bin_cent)
    plt.plot(bin_cent, result_gaus.eval(params=result_gaus.params), color="r")
    plt.xlabel(r"$\Delta r~(\mu\mathrm{m})$")
    plt.ylabel("entries")

    fig_res_rphi_z = plt.figure(figsize=(15, 7))
    ax_res_rphi_z = Axes3D(fig_res_rphi_z)
    ax_res_rphi_z.scatter(df_residuals["rphi"].to_numpy(), df_residuals["z"].to_numpy(),
                          df_residuals["residuals"].to_numpy(), cmap="turbo")
    ax_res_rphi_z.set_xlabel(r"$r\varphi~(\mathrm{mm})$")
    ax_res_rphi_z.set_ylabel(r"$z~(\mathrm{mm})$")
    ax_res_rphi_z.set_zlabel(r"$\Delta r~(\mu\mathrm{m})$")

    fig_res_rphi = plt.figure(figsize=(8, 8))
    plt.scatter(df_residuals["rphi"].to_numpy(),
                df_residuals["residuals"].to_numpy(), cmap="turbo")
    plt.xlabel(r"$r\varphi~(\mathrm{mm})$")
    plt.ylabel(r"$\Delta r~(\mu\mathrm{m})$")

    fig_res_z = plt.figure(figsize=(8, 8))
    plt.scatter(df_residuals["z"].to_numpy(),
                df_residuals["residuals"].to_numpy(), cmap="turbo")
    plt.xlabel(r"$z~(\mathrm{mm})$")
    plt.ylabel(r"$\Delta r~(\mu\mathrm{m})$")

    fig_fit, _ = show_fit(w_fit, C_fit, r_fit, data, w_fit_perp)

    print(f"\nRADIUS: {r_fit:0.2f} mm\n")
    for _, par in result_gaus.params.items():
        print(par)

    if save_figures:
        ext = ""
        if ".dat" in file_name:
            ext = ".dat"
        elif ".txt" in file_name:
            ext = ".txt"
        if ".csv" in file_name:
            ext = ".csv"

        fig_scatter.savefig(file_name.replace(ext, ".pdf"))
        fig_res_distr.savefig(file_name.replace(ext, "_residuals_distr.pdf"))
        fig_res_rphi_z.savefig(file_name.replace(ext, "_residuals_z_rphi.pdf"))
        fig_res_rphi.savefig(file_name.replace(ext, "_residuals_rphi.pdf"))
        fig_res_z.savefig(file_name.replace(ext, "_residuals_z.pdf"))
        fig_fit.savefig(file_name.replace(ext, "_fit.pdf"))

    if save_res:
        df_residuals.to_parquet(file_name.replace(
            ext, "_residuals.parquet.gzip"), compression="gzip")

    plt.show()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Arguments to pass")
    parser.add_argument("cfgFileName", metavar="text", default="cfgFileName.yml",
                        help="config file name with root input files")
    args = parser.parse_args()

    with open(args.cfgFileName, "r") as ymlCfgFile:
        inputCfg = yaml.load(ymlCfgFile, yaml.FullLoader)

    file_name = inputCfg["file_name"]
    x_min = inputCfg["measurement"]["chip_coordinates"]["x_min"]
    x_max = inputCfg["measurement"]["chip_coordinates"]["x_max"]
    y_min = inputCfg["measurement"]["chip_coordinates"]["y_min"]
    y_max = inputCfg["measurement"]["chip_coordinates"]["y_max"]
    xmax_window = inputCfg["measurement"]["window_coordinates"]["x_max"]
    ymax_window = inputCfg["measurement"]["window_coordinates"]["y_max"]
    remove_outliers = inputCfg["measurement"]["options"]["remove_outliers"]
    sampling_frac = inputCfg["measurement"]["options"]["sampling_fraction"]

    save_figures = inputCfg["output"]["figures"]
    save_res = inputCfg["output"]["tables"]

    main(file_name, x_min, x_max, y_min, y_max, xmax_window, ymax_window,
         save_figures, save_res, sampling_frac, remove_outliers)
