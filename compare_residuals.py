#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.metrics import mean_squared_error

file_names = ['chips/Clab/VR-20210401_133746_Height_refplane_high_residuals.parquet.gzip',
              'chips/Clab/MeasChipITS3_2021_4_1_residuals.parquet.gzip']

colors = ['chocolate', 'steelblue']
labels = ['Keyence', 'Mitutoyo']

residuals_array, rphi_array, z_array = [], [], []
for file_name in file_names:
    df_res = pd.read_parquet(file_name)
    df_res = df_res.sample(frac=0.2, random_state=42, replace=True, axis=0)
    residuals_array.append(df_res['residuals'].to_numpy())
    rphi_array.append(df_res['rphi'].to_numpy())
    z_array.append(df_res['z'].to_numpy())

fig_res_distr = plt.figure(figsize=(7, 7))
for res, col, lab in zip(residuals_array, colors, labels):
    rms = np.sqrt(np.mean(np.square(res)))
    lab_hist = rf'{lab}, $\mathrm{{RMS}} = {rms:0.2f}~\mu\mathrm{{m}}$'
    histo = plt.hist(res, bins=500, alpha=0.5, density=True, range=(-500, 500), color=col, label=lab_hist)
plt.xlabel(r'$\Delta r~(\mu\mathrm{m})$')
plt.ylabel('normalised entries')
plt.legend(loc='best')
plt.yscale('log')

fig_res_rphi = plt.figure(figsize=(7, 7))
for res, rphi, col, lab in zip(residuals_array, rphi_array, colors, labels):
    plt.scatter(rphi, res, alpha=0.3, color=col, label=lab)
plt.xlabel(r'$r\varphi~(\mathrm{mm})$')
plt.ylabel(r'$\Delta r~(\mu\mathrm{m})$')
plt.legend(loc='best')

fig_res_z = plt.figure(figsize=(7, 7))
for res, z, col, lab in zip(residuals_array, z_array, colors, labels):
    plt.scatter(z, res, alpha=0.3, color=col, label=lab)
plt.xlabel(r'$z~(\mathrm{mm})$')
plt.ylabel(r'$\Delta r~(\mu\mathrm{m})$')
plt.legend(loc='best')

fig_res_rphi_z = plt.figure(figsize=(15, 7))
ax_res_rphi_z = Axes3D(fig_res_rphi_z)
for res, z, rphi, col, lab in zip(residuals_array, z_array, rphi_array, colors, labels):
    ax_res_rphi_z.scatter(rphi, z, res, color=col, label=lab)
ax_res_rphi_z.set_xlabel(r'$r\varphi~(\mathrm{mm})$')
ax_res_rphi_z.set_ylabel(r'$z~(\mathrm{mm})$')
ax_res_rphi_z.set_zlabel(r'$\Delta r~(\mu\mathrm{m})$')
plt.legend(loc='best')

plt.show()
